#!/usr/bin/env python3

import pygame
import time

# Constants
LED_COUNT = 24
LED_WIDTH = 300
LED_HEIGHT = 40
LED_SPACING = 10
WINDOW_WIDTH = LED_WIDTH + 2 * LED_SPACING
WINDOW_HEIGHT = LED_COUNT * (LED_HEIGHT + LED_SPACING) + LED_SPACING
FPS = 60

# Colors
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
COLORS = [RED, GREEN, BLUE]

# Initialize Pygame
pygame.init()
screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption("Vertical RGB LED Strip Simulation")
clock = pygame.time.Clock()


# RGB LED Strip Simulation Class
class RGBLEDStrip:
    def __init__(self, num_leds):
        self.num_leds = num_leds
        self.leds = [BLACK] * num_leds

    def set_led(self, index, color):
        if 0 <= index < self.num_leds:
            self.leds[index] = color

    def draw(self, surface):
        surface.fill(BLACK)
        for i, color in enumerate(self.leds):
            pygame.draw.rect(
                surface,
                color,
                (
                    LED_SPACING,
                    LED_SPACING + i * (LED_HEIGHT + LED_SPACING),
                    LED_WIDTH,
                    LED_HEIGHT,
                ),
            )
        pygame.display.flip()

    def clear(self):
        self.leds = [BLACK] * self.num_leds


# Initialize the LED strip
strip = RGBLEDStrip(LED_COUNT)


# Additional function for color interpolation
def interpolate_color(start_color, end_color, factor):
    return tuple(
        int(start_color[i] + (end_color[i] - start_color[i]) * factor) for i in range(3)
    )


# White pulse animation function
def white_pulse(delay=0.05, pulse_duration=2):
    total_frames = int(pulse_duration / delay)
    half_frames = total_frames // 2

    for frame in range(total_frames):
        # Calculate the interpolation factor (0 to 1 and back to 0)
        if frame <= half_frames:
            factor = frame / half_frames
        else:
            factor = (total_frames - frame) / half_frames

        # Interpolated color
        color = interpolate_color(BLACK, WHITE, factor)

        # Apply color to all LEDs
        for i in range(strip.num_leds):
            strip.set_led(i, color)
        strip.draw(screen)

        time.sleep(delay)


# Function to adjust color brightness
def adjust_brightness(color, brightness):
    return tuple(int(channel * brightness) for channel in color)


# Animation function for moving blue spots
def moving_blue_spots(delay=0.05, move_duration=2):
    total_steps = int(move_duration / delay)
    for step in range(total_steps):
        # Calculate positions for the spots
        top_spot = int(step / total_steps * strip.num_leds / 2)
        bottom_spot = strip.num_leds - 1 - top_spot

        strip.clear()

        # Set the main spots
        strip.set_led(top_spot, BLUE)
        strip.set_led(bottom_spot, BLUE)

        # Set adjacent LEDs with lower brightness
        if top_spot > 0:
            strip.set_led(top_spot - 1, adjust_brightness(BLUE, 0.5))
        if top_spot < strip.num_leds - 1:
            strip.set_led(top_spot + 1, adjust_brightness(BLUE, 0.5))
        if bottom_spot > 0:
            strip.set_led(bottom_spot - 1, adjust_brightness(BLUE, 0.5))
        if bottom_spot < strip.num_leds - 1:
            strip.set_led(bottom_spot + 1, adjust_brightness(BLUE, 0.5))

        strip.draw(screen)
        time.sleep(delay)

    # Pause when spots meet in the middle
    time.sleep(0.5)


# Animation functions
def running_light(colors, delay=0.1):
    for color in colors:
        for i in range(strip.num_leds):
            strip.clear()
            strip.set_led(i, color)
            strip.draw(screen)
            time.sleep(delay)


def blinking(colors, delay=0.5):
    for _ in range(5):
        for color in colors:
            strip.clear()
            strip.draw(screen)
            time.sleep(delay)
            for i in range(strip.num_leds):
                strip.set_led(i, color)
            strip.draw(screen)
            time.sleep(delay)


# Combined animation function for moving blue spots with a white pulse in the middle
def moving_spots_with_white_pulse(delay=0.05, move_duration=2):
    total_steps = int(move_duration / delay)
    half_steps = total_steps // 2

    for step in range(total_steps):
        # Calculate positions for the spots
        top_spot = int(step / total_steps * strip.num_leds / 2)
        bottom_spot = strip.num_leds - 1 - top_spot

        strip.clear()

        # Set the main spots
        strip.set_led(top_spot, BLUE)
        strip.set_led(bottom_spot, BLUE)

        # Set adjacent LEDs with lower brightness
        if top_spot > 0:
            strip.set_led(top_spot - 1, adjust_brightness(BLUE, 0.5))
        if top_spot < strip.num_leds - 1:
            strip.set_led(top_spot + 1, adjust_brightness(BLUE, 0.5))
        if bottom_spot > 0:
            strip.set_led(bottom_spot - 1, adjust_brightness(BLUE, 0.5))
        if bottom_spot < strip.num_leds - 1:
            strip.set_led(bottom_spot + 1, adjust_brightness(BLUE, 0.5))

        # Add the white pulse in the middle when spots meet
        if step >= half_steps:
            pulse_factor = (total_steps - step) / half_steps
            middle_color = interpolate_color(BLACK, WHITE, pulse_factor)
            middle_led = strip.num_leds // 2
            strip.set_led(middle_led, middle_color)

            if middle_led > 0:
                strip.set_led(middle_led - 1, adjust_brightness(middle_color, 0.5))
            if middle_led < strip.num_leds - 1:
                strip.set_led(middle_led + 1, adjust_brightness(middle_color, 0.5))

        strip.draw(screen)
        time.sleep(delay)


# Combined animation function for moving blue spots with a white pulse in the middle
def moving_spots_with_white_pulse(delay=0.05, move_duration=2, pulse_duration=1):
    total_steps = int(move_duration / delay)
    pulse_steps = int(pulse_duration / delay)

    for step in range(total_steps + pulse_steps):
        strip.clear()

        if step < total_steps:
            # Calculate positions for the spots
            top_spot = int(step / total_steps * strip.num_leds / 2)
            bottom_spot = strip.num_leds - 1 - top_spot

            # Set the main spots
            strip.set_led(top_spot, BLUE)
            strip.set_led(bottom_spot, BLUE)

            # Set adjacent LEDs with lower brightness
            if top_spot > 0:
                strip.set_led(top_spot - 1, adjust_brightness(BLUE, 0.5))
            if top_spot < strip.num_leds - 1:
                strip.set_led(top_spot + 1, adjust_brightness(BLUE, 0.5))
            if bottom_spot > 0:
                strip.set_led(bottom_spot - 1, adjust_brightness(BLUE, 0.5))
            if bottom_spot < strip.num_leds - 1:
                strip.set_led(bottom_spot + 1, adjust_brightness(BLUE, 0.5))
        else:
            # Start white pulse after spots have met
            pulse_factor = (pulse_steps - (step - total_steps)) / pulse_steps
            middle_color = interpolate_color(BLACK, WHITE, pulse_factor)
            middle_led = strip.num_leds // 2
            strip.set_led(middle_led, middle_color)

            # Set adjacent LEDs with lower brightness
            strip.set_led(middle_led - 2, adjust_brightness(middle_color, 0.3))
            strip.set_led(middle_led - 1, adjust_brightness(middle_color, 0.7))
            strip.set_led(middle_led + 1, adjust_brightness(middle_color, 0.7))
            strip.set_led(middle_led + 2, adjust_brightness(middle_color, 0.3))

        strip.draw(screen)
        time.sleep(delay)


# Main loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # running_light(COLORS, 0.1)
    # blinking(COLORS, 0.5)
    # white_pulse(0.05, 0.5)
    # moving_blue_spots(0.05, 1)  # Adjust these parameters as needed
    # moving_spots_with_white_pulse(0.05, 2)  # Adjust these parameters as needed
    moving_spots_with_white_pulse(0.05, 1, 0.5)  # Adjust these parameters as needed

    clock.tick(FPS)

pygame.quit()
